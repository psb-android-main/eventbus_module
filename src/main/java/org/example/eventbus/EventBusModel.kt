package org.example.eventbus

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

object EventBusModel  {

    private val _events = MutableSharedFlow<MessageEvent>() // private mutable shared flow
    val events = _events.asSharedFlow() // publicly exposed as read-only shared flow

    suspend fun produceEventSus(event: MessageEvent) {
        _events.emit(event) // suspends until all subscribers receive it
    }
}