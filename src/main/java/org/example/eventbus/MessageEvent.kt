package org.example.eventbus

sealed class MessageEvent {
    data class MessageDeposit(val message: String = "") : MessageEvent()
    data class MessageCredit(val message: String = "") : MessageEvent()
    data class MessageAmount(val amount: Double) : MessageEvent()
    data class MessageTypeCredit(val type: TypeCredit) : MessageEvent()
}