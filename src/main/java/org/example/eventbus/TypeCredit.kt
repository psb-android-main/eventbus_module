package org.example.eventbus

// type of credit
enum class TypeCredit {
    CONSUMER, MORTGAGE
}